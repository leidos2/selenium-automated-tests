To run the automation scripts you will need to install node.js and npm.

The tests also run in firefox so you will also need to add geckodriver to your path. The geckodriver.exe file can be found in utils/driver.

Once everything is configured and intalled correctly run the following commands in the root directory to install all dependencies and start the tests.

### npm install

### node index.js
