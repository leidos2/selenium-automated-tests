const {Builder} = require('selenium-webdriver');
const BasicSearch = require('./tests/basicSearch.js');
const AdvancedSearch = require('./tests/advSearch.js');

(async function main() {
    let driver = await new Builder().forBrowser('firefox').build();
    try {
        // initialize test classes
        let basicSearch = new BasicSearch(driver);
        let advSearch = new AdvancedSearch(driver);
        // verify at least one result appearing for searching all results in the database
        await basicSearch.testSearchAllResults();
        await advSearch.testSearchQuery('govern');
    }
    finally{
        driver.quit();
    }
})();
