const {Builder, By, Key, until} = require('selenium-webdriver');

class AdvancedSearch {

    constructor(driver) {
        this.driver = driver;
    }
    
     async testSearchQuery(query) {
        // Navigate to parlinfo
        await this.driver.get('https://parlinfo.aph.gov.au');

        // navigate to advanced search
        const advBrowseBtn = '//*[@id="secondaryNav"]/ul/li[2]/a/span';
        await this.driver.findElement(By.xpath(advBrowseBtn)).click();

        // enter search query
        await this.driver.findElement(By.id('searchText')).sendKeys(query, Key.ENTER);

        // wait for searched results to appear.
        await this.driver.wait(until.elementLocated(By.className('result hiliteRow')), 10000);
        
        // verify query is correct for first search result:
        const titleElemPath = '//*[@id="results"]/li[1]/div[3]/div[1]/a/span/strong'
        const text = await this.driver.findElement(By.xpath(titleElemPath)).getText();
        console.assert(text.includes(query), 'TEXT FAILURE: search results did not match query');
    }
}

module.exports = AdvancedSearch;