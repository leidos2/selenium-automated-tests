const {Builder, By, Key, until} = require('selenium-webdriver');

class BasicSearch {

    constructor(driver) {
        this.driver = driver;
    }

    test() {
        console.log('it worked!');
    }

     async testSearchAllResults() {
        // Navigate to Url
        await this.driver.get('https://parlinfo.aph.gov.au');

        // Enter text "cheese" and perform keyboard action "Enter"
        await this.driver.findElement(By.id('query')).sendKeys('?', Key.ENTER);

        // wait for searched results to appear.
        await this.driver.wait(until.elementLocated(By.className('result hiliteRow')), 10000);

        //console.log(await firstResult.getAttribute('textContent'));
    }
}

module.exports = BasicSearch;